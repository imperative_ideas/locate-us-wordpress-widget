<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @package   Business Location Widget w/Google Schema & Maps
 * @author    Imperative Ideas (@imperativeideas)
 * @license   GPL-2.0+
 * @link      http://imperativeideas.com
 * @copyright 2014 Ian Armstrong / Imperative Ideas
 */

/**
 * We need to initialize the plugin by checking for jQuery. If it's not enqueued, add it
 * This technique avoids duplication, provided the theme developer has not manually
 * hard-coded jQuery the wrong way.
 */

add_action( 'wp_enqueue_scripts', 'loc_enqueue_jquery',99 );
function loc_enqueue_jquery(){ // By using enqueue, we ensure this does not create a duplicate jQuery
    if( !is_admin() )
        wp_enqueue_script('jquery');
}

