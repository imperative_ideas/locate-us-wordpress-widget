<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @package   Business Location Widget w/Google Schema & Maps
 * @author    Imperative Ideas (@imperativeideas)
 * @license   GPL-2.0+
 * @link      http://imperativeideas.com
 * @copyright 2014 Ian Armstrong / Imperative Ideas
 */

//if uninstall not called from WordPress exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
exit();

// @TODO: Define uninstall functionality here