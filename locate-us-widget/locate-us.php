<?php
/*
Plugin Name: Business Location Widget w/Google Schema
Plugin URI: http://imperativeideas.com/widgets/locate-us
Description: A Widgetized and XML Schema compliant location widget for small businesses
Version: 1.0.5
Author: Imperative Ideas
Author URI: http://imperativeideas.com
License: GPL
*/

add_action( 'widgets_init', 'locate_us_widget' );
load_plugin_textdomain('locate_us', false, basename( dirname( __FILE__ ) ) . '/languages' );

function locate_us_widget() {
    register_widget( 'Locate_Us_Widget' );
}

class Locate_Us_Widget extends WP_Widget {

    function Locate_Us_Widget() { // Constructor
        $widget_ops = array( 'classname' => 'locate_us', 'description' => __('A GUI Widget for controlling the Locate Us sidebar.', 'locate_us') );
        $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'locate-us' );
        parent::WP_Widget( 'locate-us', __('Locate Us', 'locate_us'), $widget_ops, $control_ops );
        include 'inc/init.php';
    }

    function widget( $args, $instance ) { // widget form creation
        extract( $args );

        //Our variables from the widget settings.

        $title = isset( $instance['title'] ) ? $instance['title'] : false;
        $locationtype = isset( $instance['location_type'] ) ? $instance['location_type'] : false;
        $orgname = isset( $instance['orgname'] ) ? $instance['orgname'] : false;
        $address_1 = isset( $instance['address_1'] ) ? $instance['address_1'] : false;
        $address_2 = isset( $instance['address_2'] ) ? $instance['address_2'] : false;
        $address3 = isset( $instance['address_3'] ) ? $instance['address_3'] : false;
        $city = isset( $instance['city'] ) ? $instance['city'] : false;
        $state = isset( $instance['state'] ) ? $instance['state'] : false;
        $zip = isset( $instance['zip'] ) ? $instance['zip'] : false;
        $phone = isset( $instance['phone'] ) ? $instance['phone'] : false;
        $email = isset( $instance['email'] ) ? $instance['email'] : false;
        $show_map = isset( $instance['show_map'] ) ? $instance['show_map'] : false;
        $show_map_title = isset( $instance['show_map_title'] ) ? $instance['show_map_title'] : false;
        $map_title = isset( $instance['map_title'] ) ? $instance['map_title'] : false;
        $map_width = isset( $instance['map_width'] ) ? $instance['map_width'] : false;
        $map_height = isset( $instance['map_height'] ) ? $instance['map_height'] : false;
        $map_type = isset( $instance['map_type'] ) ? $instance['map_type'] : false;
        $map_style = !empty( $instance['map_style'] ) ? $instance['map_style'] : false;
        $zoom_level = isset( $instance['zoom'] ) ? $instance['zoom'] : false;
        $pan_control = isset( $instance['pan_control'] ) ? $instance['pan_control'] : false;
        $zoom_control = isset( $instance['zoom_control'] ) ? $instance['zoom_control'] : false;
        $type_control = isset( $instance['type_control'] ) ? $instance['type_control'] : false;
        $scale_control = isset( $instance['scale_control'] ) ? $instance['scale_control'] : false;
        $street_control = isset( $instance['street_control'] ) ? $instance['street_control'] : false;
        $over_control = isset( $instance['over_control'] ) ? $instance['over_control'] : false;
        $rotate_control = isset( $instance['rotate_control'] ) ? $instance['rotate_control'] : false;

        echo $before_widget;

        global $exists;

        //Display the Title
        if ( $title )
            printf( '<h3 class="loc-cta">' . $title . '</h3>', $name );

        //Check if address fields are set and if so, open an unordered list
        if ( $address_1 || $address_2 || $city || $state || $zip || $phone || $email )
            $exists = true; // flag so we can close this later
            printf( '<div itemscope itemtype="' . $locationtype . '">' );
            printf( '<ul class="loc-schema">' );

        if ( $orgname )
            printf( '<li class="loc-org"><span itemprop="name">' . $orgname . '</span></li>' );

        if ( $address_1 && $city && $state && $zip )                    // Open Postal Address
            printf( '<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">');

        if ( $address_1 ) {
            if ( $address_2 ) {
                printf( '<li class="loc-address loc-address-1"><span itemprop="streetAddress">' . $address_1 . '</li>' );
            } else {
                printf( '<li class="loc-address loc-address-1"><span itemprop="streetAddress">' . $address_1 . '</span></li>' );
            }
        }

        if ( $address_2 )
            printf( '<li class="loc-address loc-address-2">' . $address_2 . '</span></li>' );

        if ( $city || $state || $zip )
            printf( '<li>' );

        if ( $city )
            printf( '<span class="loc-city" itemprop="addressLocality">' . $city . '</span> ' );

        if ( $state )
            printf ( '<spanclass="loc-state" itemprop="addressRegion">' . $state . '</span>, ');

        if ( $zip )
            printf( '<spanclass="loc-postal-code" itemprop="postalCode">'. $zip );

        if ( $state || $city || $zip )
            printf( '</li>' );

        if ( $address_1 && $city && $state && $zip )                    // close Postal Address
            printf( '</span>');

        if ( $phone )
            printf( '<li class="loc-phone"><span itemprop="telephone">Phone: ' . $phone . '</span></li>' );

        if ( $email )
            printf( '<li class="loc-email"><a href="mailto:'. $email . '">' . '<span itemprop="email">' . $email . '</span></a></li>' );

        // Close out our list tag
        if ( $exists == true )
            printf( '</ul>' );

        if ( $show_map ) {
            if( $show_map_title == true && !empty( $map_title )) : ?>
            <h3><?php echo $map_title; ?></h3>
            <?php endif; ?>
            <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
            <script type="text/javascript">
                var geocoder;
                var map;

                function loadmap() { // Load the map

                    <?php /* @todo Load only the needed style from an .inc file, including all of them is inefficient*/ ?>
                    <?php /* @todo Need to skip all styling commands if "Road Map" is not selected and make their display conditional in the admin*/ ?>

                    <?php if($map_style != 'default' && $map_type == 'ROADMAP') : ?>
                    // Load Styles
                    var appleMaps = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]}];
                    var avacadoWorld = [{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#aee2e0"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#abce83"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#769E72"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#7B8758"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#EBF4A4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#8dab68"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5B5B3F"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ABCE83"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#A4C67D"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#9BBF72"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#EBF4A4"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#87ae79"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#7f2200"},{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":4.1}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#495421"}]},{"featureType":"administrative.neighborhood","elementType":"labels","stylers":[{"visibility":"off"}]}];
                    var blueGray = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#b5cbe4"}]},{"featureType":"landscape","stylers":[{"color":"#efefef"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#83a5b0"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#bdcdd3"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e3eed3"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}];
                    var blueWater = [{"featureType":"water","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]}];
                    var lightGreen = [{"stylers":[{"hue":"#baf4c4"},{"saturation":10}]},{"featureType":"water","stylers":[{"color":"#effefd"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]}];
                    var flatGreen = [{"stylers":[{"hue":"#bbff00"},{"weight":0.5},{"gamma":0.5}]},{"elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","stylers":[{"color":"#a4cc48"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":1}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"},{"gamma":1.14},{"saturation":-18}]},{"featureType":"road.highway.controlled_access","elementType":"labels","stylers":[{"saturation":30},{"gamma":0.76}]},{"featureType":"road.local","stylers":[{"visibility":"simplified"},{"weight":0.4},{"lightness":-8}]},{"featureType":"water","stylers":[{"color":"#4aaecc"}]},{"featureType":"landscape.man_made","stylers":[{"color":"#718e32"}]},{"featureType":"poi.business","stylers":[{"saturation":68},{"lightness":-61}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"weight":2.7},{"color":"#f4f9e8"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"weight":1.5},{"color":"#e53013"},{"saturation":-42},{"lightness":28}]}];
                    var lunarLand = [{"stylers":[{"hue":"#ff1a00"},{"invert_lightness":true},{"saturation":-100},{"lightness":33},{"gamma":0.5}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2D333C"}]}];
                    var mapBox = [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}];
                    var midnight = [{"featureType":"water","stylers":[{"color":"#021019"}]},{"featureType":"landscape","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#0b434f"},{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"transit","stylers":[{"color":"#146474"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]}];
                    var paleDawn = [{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"landscape","stylers":[{"color":"#f2e5d4"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"administrative","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"road"},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{},{"featureType":"road","stylers":[{"lightness":20}]}];
                    var paper = [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"color":"#5f94ff"},{"lightness":26},{"gamma":5.86}]},{},{"featureType":"road.highway","stylers":[{"weight":0.6},{"saturation":-85},{"lightness":61}]},{"featureType":"road"},{},{"featureType":"landscape","stylers":[{"hue":"#0066ff"},{"saturation":74},{"lightness":100}]}];
                    var redHues = [{"stylers":[{"hue":"#dd0d0d"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]}];
                    var subtleGray = [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}];
                    var turquoise = [{"stylers":[{"hue":"#16a085"},{"saturation":0}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]}];

                    // Create a new StyledMapType object, passing it the array of styles,
                    // as well as the name to be displayed on the map type control.
                    var styledMap = new google.maps.StyledMapType(<?php if (!empty($map_style)) { echo $map_style; } else { echo "paleDawn"; } ?>,
                        {name: "<?php if (!empty($orgname)) { echo $orgname; } else { echo "Style: " . $map_style; } ?>"});

                    <?php endif; ?>
                    var address = '<?php echo "$address_1 " . "$city " . "$state " . "$zip" ?>';
                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode( { 'address': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var location = results[0].geometry.location;
                            lat = location.lat();
                            lng = location.lng();

                            var latlng = new google.maps.LatLng(lat, lng);
                            var mapOptions = {
                                zoom: <?php echo $zoom_level; ?>,
                                center: latlng,
                                <?php if($map_style == 'default' || $map_type != 'ROADMAP') : ?>
                                mapTypeId: google.maps.MapTypeId.<?php echo $map_type; ?>,
                                <?php else : ?>
                                mapTypeIds: [google.maps.MapTypeId.<?php echo $map_type; ?>, 'map_style'],
                                <?php endif; ?>
                                panControl:<?php echo $pan_control; ?>,
                                zoomControl:<?php echo $zoom_control; ?>,
                                mapTypeControl:<?php echo $type_control; ?>,
                                scaleControl:<?php echo $scale_control; ?>,
                                streetViewControl:<?php echo $street_control; ?>,
                                overviewMapControl:<?php echo $over_control; ?>,
                                rotateControl:<?php echo $rotate_control; ?>
                            };

                            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                title: "<?php echo $orgname; ?>"
                            });

                            //Associate the styled map with the MapTypeId and set it to display.
                            map.mapTypes.set('map_style', styledMap);
                            map.setMapTypeId('map_style');
                        }
                    });
                }

                window.onload = loadmap;
            </script>

            <div id="map-canvas" style="width:<?php echo $map_width; ?>px; height:<?php echo $map_height; ?>px;"></div>

        <?php }

        echo $after_widget;
    }

    function update( $new_instance, $old_instance ) {           //Update the widget
        $instance = $old_instance;

        //Strip tags from text input sanitize HTML
        $instance['title'] = htmlspecialchars( $new_instance['title'] );
        $instance['location_type'] = htmlspecialchars( $new_instance['location_type'] );
        $instance['orgname'] = htmlspecialchars( $new_instance['orgname'] );
        $instance['address_1'] = htmlspecialchars( $new_instance['address_1'] );
        $instance['address_2'] = htmlspecialchars( $new_instance['address_2'] );
        $instance['address_3'] = htmlspecialchars( $new_instance['address_3'] );
        $instance['city'] = htmlspecialchars( $new_instance['city'] );
        $instance['state'] = htmlspecialchars( $new_instance['state'] );
        $instance['zip'] = htmlspecialchars( $new_instance['zip'] );
        $instance['phone'] = htmlspecialchars( $new_instance['phone'] );
        $instance['email'] = htmlspecialchars( $new_instance['email'] );
        $instance['show_map'] = $new_instance['show_map'];
        $instance['show_map_title'] = $new_instance['show_map_title'];
        $instance['map_title'] = htmlspecialchars( $new_instance['map_title'] );
        $instance['zoom'] = htmlspecialchars( $new_instance['zoom'] );
        $instance['pan_control'] = htmlspecialchars( $new_instance['pan_control'] );
        $instance['zoom_control'] = htmlspecialchars( $new_instance['zoom_control'] );
        $instance['type_control'] = htmlspecialchars( $new_instance['type_control'] );
        $instance['scale_control'] = htmlspecialchars( $new_instance['scale_control'] );
        $instance['street_control'] = htmlspecialchars( $new_instance['street_control'] );
        $instance['over_control'] = htmlspecialchars( $new_instance['over_control'] );
        $instance['rotate_control'] = htmlspecialchars( $new_instance['rotate_control'] );
        $instance['map_width'] = htmlspecialchars( $new_instance['map_width'] );
        $instance['map_height'] = htmlspecialchars( $new_instance['map_height'] );
        $instance['map_type'] = htmlspecialchars( $new_instance['map_type'] );
        $instance['map_style'] = htmlspecialchars( $new_instance['map_style']);

        return $instance;
    }

    function form( $instance ) {                                // Display the widget
        include 'inc/arrays.inc';

        //Set up some default widget settings.
        $defaults = array(
            'title' => __('Contact Us'), 'locate_us',
            'location_type' => __('Organization'), 'locate_us',
            'orgname' => '',
            'address_1' => '',
            'address_2' => '',
            'address_3' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'phone' => '',
            'email' => '',
            'show_map' => false,
            'show_map_title' => false,
            'map_title' => __('Location'), 'locate_us',
            'zoom' => '15',
            'pan_control' => false,
            'zoom_control' => false,
            'type_control' => false,
            'scale_control' => false,
            'street_control' => false,
            'over_control' => false,
            'rotate_control' => false,
            'map_width' => '190',
            'map_height' => '190',
            'map_type' => 'ROADMAP',
            'map_style' => 'default'
        );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <?php // Administrative Form Inputs ?>

        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('location_type'); ?>"><?php _e('Type Of Organization', 'locate_us'); ?></label>
            <select name="<?php echo $this->get_field_name('location_type'); ?>" id="<?php echo $this->get_field_id('location_type'); ?>" class="widefat">
                <?php
                foreach ($orgtype as $org) {
                    echo '<option value="' . $org . '" id="' . $org . '"', $instance['location_type'] == $org ? ' selected="selected"' : '', '>', $org, '</option>';
                }
                ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'orgname' ); ?>"><?php _e('Name of Your Business or Group:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'orgname' ); ?>" name="<?php echo $this->get_field_name( 'orgname' ); ?>" value="<?php echo $instance['orgname']; ?>" style="width:100%;" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'address_1' ); ?>"><?php _e('Address Line 1:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'address_1' ); ?>" name="<?php echo $this->get_field_name( 'address_1' ); ?>" value="<?php echo $instance['address_1']; ?>" style="width:100%;" />
            <label for="<?php echo $this->get_field_id( 'address_2' ); ?>"><?php _e('Address Line 2:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'address_2' ); ?>" name="<?php echo $this->get_field_name( 'address_2' ); ?>" value="<?php echo $instance['address_2']; ?>" style="width:100%;" />
            <label for="<?php echo $this->get_field_id( 'address_3' ); ?>"><?php _e('Address Line 3:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'address_3' ); ?>" name="<?php echo $this->get_field_name( 'address_3' ); ?>" value="<?php echo $instance['address_3']; ?>" style="width:100%;" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'city' ); ?>"><?php _e('City:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'city' ); ?>" name="<?php echo $this->get_field_name( 'city' ); ?>" value="<?php echo $instance['city']; ?>" style="width:100%;" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('state'); ?>"><?php _e('State', 'locate_us'); ?></label>
            <select name="<?php echo $this->get_field_name('state'); ?>" id="<?php echo $this->get_field_id('state'); ?>" class="widefat">
                <?php
                foreach ($states as $abv => $full) {
                    echo '<option value="' . $abv . '" id="' . $abv . '"', $instance['state'] == $abv ? ' selected="selected"' : '', '>', $full, '</option>';
                }
                ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'zip' ); ?>"><?php _e('Zip:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'zip' ); ?>" name="<?php echo $this->get_field_name( 'zip' ); ?>" value="<?php echo $instance['zip']; ?>" style="width:100%;" />
        </p>

        <p>
            <input class="checkbox" type="checkbox" <?php checked( $instance['show_map'], 'on' ); ?> id="<?php echo $this->get_field_id( 'show_map' ); ?>" name="<?php echo $this->get_field_name( 'show_map' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_map' ); ?>"><?php _e('Show a map of this location? (must include a valid address above for this to work)', 'locate_us'); ?></label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone Number:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" style="width:100%;" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('Email Address:', 'locate_us'); ?></label>
            <input id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo $instance['email']; ?>" style="width:100%;" />
        </p>

        <h3>Map Controls</h3>
        <div id="location-map">
            <hr />

            <p>
                <input class="checkbox" type="checkbox" <?php checked( $instance['show_map_title'], 'on' ); ?> id="<?php echo $this->get_field_id( 'show_map_title' ); ?>" name="<?php echo $this->get_field_name( 'show_map_title' ); ?>" />
                <label for="<?php echo $this->get_field_id( 'show_map_title' ); ?>"><?php _e('Show a header above the map?', 'locate_us'); ?></label>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'map_title' ); ?>"><?php _e('Map Header:', 'locate_us'); ?></label>
                <input id="<?php echo $this->get_field_id( 'map_title' ); ?>" name="<?php echo $this->get_field_name( 'map_title' ); ?>" value="<?php echo $instance['map_title']; ?>" style="width:100%;" />
            </p>

            <p>
                <label for="<?php echo $this->get_field_id('map_type'); ?>"><?php _e('Map Type', 'locate_us'); ?></label>
                <select name="<?php echo $this->get_field_name('map_type'); ?>" id="<?php echo $this->get_field_id('map_type'); ?>" class="widefat">
                    <?php
                    foreach ($types as $code => $human) {
                        echo '<option value="' . $code . '" id="' . $code . '"', $instance['map_type'] == $code ? ' selected="selected"' : '', '>', $human, '</option>';
                    } ?>
                </select>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id('map_style'); ?>"><?php _e('Map Style', 'locate_us'); ?></label>
                <select name="<?php echo $this->get_field_name('map_style'); ?>" id="<?php echo $this->get_field_id('map_style'); ?>" class="widefat">
                    <?php
                    foreach ($styles as $style => $title) {
                        echo '<option value="' . $style . '" id="' . $style . '"', $instance['map_style'] == $style ? ' selected="selected"' : '', '>', $title, '</option>';
                    } ?>
                </select>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id('zoom'); ?>"><?php _e('Map Zoom Level', 'locate_us'); ?></label>
                <select name="<?php echo $this->get_field_name('zoom'); ?>" id="<?php echo $this->get_field_id('zoom'); ?>" class="widefat">
                    <?php
                    $count = 0;
                    while ($count < 22) { // let's knock out 0-21 with a simple counter
                        echo '<option value="' . $count . '" id="' . $count . '"', $instance['zoom'] == $count ? ' selected="selected"' : '', '>', $count, '</option>';
                        ++$count;
                    } ?>
                </select>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'map_width' ); ?>"><?php _e('Map Width:', 'locate_us'); ?></label>
                <input id="<?php echo $this->get_field_id( 'map_width' ); ?>" name="<?php echo $this->get_field_name( 'map_width' ); ?>" value="<?php echo $instance['map_width']; ?>" style="width:50%;" />
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'map_height' ); ?>"><?php _e('Map Height:', 'locate_us'); ?></label>
                <input id="<?php echo $this->get_field_id( 'map_height' ); ?>" name="<?php echo $this->get_field_name( 'map_height' ); ?>" value="<?php echo $instance['map_height']; ?>" style="width:50%;" />
            </p>

            <div style="padding: 8px; margin-bottom: 5px; border: 1px solid silver; background: whitesmoke;">
            <p><strong>Map Controls:</strong> <em>May not appear on small maps</em></p>

            <ul>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'zoom_control' ); ?>" name="<?php echo $this->get_field_name( 'zoom_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['zoom_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'zoom_control' ); ?>" name="<?php echo $this->get_field_name( 'zoom_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'zoom_control' ); ?>"><?php _e('Show the Zoom Control?', 'locate_us'); ?></label>
                </li>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'pan_control' ); ?>" name="<?php echo $this->get_field_name( 'pan_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['pan_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'pan_control' ); ?>" name="<?php echo $this->get_field_name( 'pan_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'pan_control' ); ?>"><?php _e('Show the Pan Control?', 'locate_us'); ?></label>
                </li>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'type_control' ); ?>" name="<?php echo $this->get_field_name( 'type_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['type_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'type_control' ); ?>" name="<?php echo $this->get_field_name( 'type_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'type_control' ); ?>"><?php _e('Show the Map Type Control?', 'locate_us'); ?></label>
                </li>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'scale_control' ); ?>" name="<?php echo $this->get_field_name( 'scale_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['scale_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'scale_control' ); ?>" name="<?php echo $this->get_field_name( 'scale_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'scale_control' ); ?>"><?php _e('Show the Scale Control?', 'locate_us'); ?></label>
                </li>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'street_control' ); ?>" name="<?php echo $this->get_field_name( 'street_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['street_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'street_control' ); ?>" name="<?php echo $this->get_field_name( 'street_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'street_control' ); ?>"><?php _e('Show the Street View Control?', 'locate_us'); ?></label>
                </li>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'over_control' ); ?>" name="<?php echo $this->get_field_name( 'over_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['over_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'over_control' ); ?>" name="<?php echo $this->get_field_name( 'over_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'over_control' ); ?>"><?php _e('Show the Overview Thumbnail?', 'locate_us'); ?></label>
                </li>
                <li>
                    <input type="hidden" id="<?php echo $this->get_field_id( 'rotate_control' ); ?>" name="<?php echo $this->get_field_name( 'rotate_control' ); ?>" value="false" />
                    <input class="checkbox" type="checkbox" <?php checked( $instance['rotate_control'], 'true' ); ?> id="<?php echo $this->get_field_id( 'rotate_control' ); ?>" name="<?php echo $this->get_field_name( 'rotate_control' ); ?>" value="true" />
                    <label for="<?php echo $this->get_field_id( 'rotate_control' ); ?>"><?php _e('Show the Rotate Control?', 'locate_us'); ?></label>
                </li>
            </ul>
            </div>

            <div id="map-canvas" style="width:<?php if(!empty($map_width)) : echo $map_width; endif; ?>px; height:<?php if(!empty($map_height)) : echo $map_height; endif; ?>px;"></div>

        </div>

    <?php
    }
}